@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
			<form action="/posts/{{$post->id}}/like" method="POST" class="d-inline">
				@method('PUT')
				@csrf
				@if($post->likes->contains('user_id', Auth::id()))
				<button type="submit" class="btn btn-danger">Unlike</button>
				@else
				<button type="submit" class="btn btn-success">Like</button>
				@endif
				

			</form>
			@endif
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
				Post Comment
			  </button>
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	{{-- <div class="card mt-4">
		<div class="card-body">
			<h2 class="card-title">Comments</h2>
			<p class="card-subtitle text-muted">{{$comment}}</p>
		</div>
	</div> --}}

	<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title" id="staticBackdropLabel">Post Comment</h5>
			  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			  <form action="/posts/{{$post->id}}/comment" method="POST">
				@csrf
				<div class="form-group my-3">
					<label for="content">Content</label>
					<textarea class="form-control" name="content" id="content" rows="3"></textarea>
				  </div>
				
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Post</button>
			</form>
			</div>
			
		  </div>
		</div>
	  </div>
@endsection